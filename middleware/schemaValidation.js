const { validationResult } = require("express-validator");
const schemaValidation = async (req, res, next) => {
  const result = validationResult(req);
  //   const errors = result.array();
  const result2 = result.formatWith((error) => error.msg);
  const errors = result2.array();
  console.log(result);
  if (result.isEmpty()) {
    next();
  } else {
    res.status(400).json({ errors });
  }
};
module.exports = schemaValidation;
