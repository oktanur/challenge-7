const jwt = require("jsonwebtoken");
require("dotenv").config();
const authMiddleware = async (req, res, next) => {
  // check if authorization is in header
  // get authorization key in header
  const { authorization } = req.headers;
  if (authorization === undefined) {
    return res.status(401).json({ message: "Unauthorized" });
  }
  try {
    // verify token
    const splitedToken = authorization.split(" ")[1];
    const token = await jwt.verify(splitedToken, process.env.SECRET_KEY);
    // save token variable in request for protection middleware;
    req.user = token;
    next();
  } catch (err) {
    return res.status(400).json({ message: "Invalid token" });
  }
};
module.exports = authMiddleware;
