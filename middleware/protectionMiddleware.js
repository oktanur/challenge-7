const protectionMiddleware = async (req, res, next) => {
  const myTokenId = req.user.id.toString();
  const { idUser } = await req.params;
  if (myTokenId === idUser) {
    next();
  } else {
    return res.status(401).json({ message: "unauthorized" });
  }
};
module.exports = protectionMiddleware;
