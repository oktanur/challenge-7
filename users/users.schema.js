// register validations
const registerValidation = {
  username: {
    isEmpty: {
      errorMessage: "Please input your username",
      negated: true,
      options: [{ ignore_whitespace: true }],
    },
    custom: {
      errorMessage: "Username shouldn't contain whitespace",
      options: (value) => !/\s/.test(value),
    },
  },
  email: {
    isEmpty: {
      negated: true,
      errorMessage: "Please input your email address",
    },
    isEmail: {
      errorMessage: "Invalid email format",
    },
  },
  password: {
    isEmpty: {
      negated: true,
      errorMessage: "Please input your password",
    },
    isLength: {
      errorMessage: "Your password must contains at least 8 character",
      options: { min: 8 },
    },
  },
};

// login validation
const loginValidation = {
  email: {
    isEmpty: {
      negated: true,
      errorMessage: "Please input your email address",
    },
    isEmail: {
      errorMessage: "Invalid email format",
    },
  },
  password: {
    isEmpty: {
      negated: true,
      errorMessage: "Please input your password",
    },
  },
};

// user biodata validation
const bioValidation = {
  fullname: {
    optional: {
      options: { nullable: true },
    },
    isString: {
      negated: false,
      errorMessage: "Gender should be string",
    },
    isEmpty: {
      errorMessage: "Fullname is empty",
      negated: true,
      options: [{ ignore_whitespace: true }],
    },
  },
  phoneNumber: {
    isEmpty: {
      errorMessage: "Phone number is empty",
      negated: true,
      options: [{ ignore_whitespace: true }],
    },
    isMobilePhone: {
      errorMessage: "Invalid phone number",
      options: ["any", { strictMode: true }],
    },
    optional: {
      options: { nullable: true },
    },
  },
  dateOfBirth: {
    isEmpty: {
      errorMessage: "Date of birth is empty",
      negated: true,
      options: [{ ignore_whitespace: true }],
    },
    isDate: {
      errorMessage: "Invalid date format use:yyyy-mm-dd",
      options: [{ format: "yyyy-mm-dd" }],
    },
    optional: {
      options: { nullable: true },
    },
  },
  gender: {
    optional: {
      options: { nullable: true },
    },
    isEmpty: {
      errorMessage: "Gender is empty",
      negated: true,
      options: [{ ignore_whitespace: true }],
    },
    isString: {
      negated: false,
      errorMessage: "Gender should be string",
    },
    isIn: {
      errorMessage: "Gender should be male or female",
      options: [["male", "female"]],
    },
  },
};
module.exports = { registerValidation, loginValidation, bioValidation };
