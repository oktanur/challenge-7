// import user.model.js
const userModel = require("./users.model");
const jwt = require("jsonwebtoken");
require("dotenv").config();

class UserController {
  // render register page
  renderRegister = (req, res) => {
    res.render("register");
  };

  //   render login page
  renderLogin = (req, res) => {
    res.render("login");
  };

  //get all users
  getAllUsers = async (req, res) => {
    try {
      const allUsers = await userModel.getUsers();
      return res.status(200).json(allUsers);
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  // register new user
  registerUser = async (req, res) => {
    try {
      let { username, email, password } = req.body;
      // find whether user exists or not. If username exists, it will return true
      const existUser = await userModel.isRegistered(username, email);
      // condition when user exists
      if (existUser) {
        return res
          .status(400)
          .json({ message: "Username or Email already exists" });
      }
      // record new user if user doesn't exists
      await userModel.recordUser(username, email, password);
      return res.status(200).json({ message: "New user is recorded" });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };

  // login user
  loginUser = async (req, res) => {
    let { email, password } = req.body;
    try {
      // find user with email and password
      const sortedUsers = await userModel.authenticateUser(email, password);
      // condition when email and password match
      if (sortedUsers) {
        // generate token and concat sortedUsers
        const token = jwt.sign({ ...sortedUsers }, process.env.SECRET_KEY, {
          expiresIn: "1d",
        });
        return res.status(200).json({ accessToken: token });
      } else {
        // condition when email and password don't match
        return res.status(400).json({ message: "Invalid credentials" });
      }
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };

  // create or update userBios
  createOrUpdateBio = async (req, res) => {
    try {
      const { idUser } = req.params;
      const { fullname, gender, address, phoneNumber, dateOfBirth } = req.body;
      // find user_id in UserBios table
      const userBioId = await userModel.getUserById(idUser);
      // condition when user_id in UserBios table doesn't exist
      if (!userBioId) {
        // create new userBios with specified user_id
        await userModel.createUserBio(
          fullname,
          gender,
          address,
          phoneNumber,
          dateOfBirth,
          idUser
        );
        return res.status(200).json({ message: "New user bio is created" });
      } else {
        // condition when user_id in UserBios exists
        // update userBios table with specified user_id
        await userModel.updateUserBio(
          fullname,
          gender,
          address,
          phoneNumber,
          dateOfBirth,
          idUser
        );
        return res.status(200).json({ message: "User bio is updated" });
      }
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };

  // get single user bio with specified user_id
  getUserBio = async (req, res) => {
    try {
      const { idUser } = req.params;
      // get user_id from UserBios table
      const user = await userModel.getUserById(idUser);
      // condition when user_id in UsrBios table exists
      if (user) {
        return res.status(200).json(user);
      } else {
        // condition when user_id in UserBios table does not exist
        return res
          .status(400)
          .json({ message: `biodata with user id ${idUser} is not found` });
      }
    } catch (err) {
      console.log(er);
      return res.status(500).json({ message: "Internal server error" });
    }
  };
}
module.exports = new UserController();
