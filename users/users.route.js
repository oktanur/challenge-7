const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");
const path = require("path");
const authMiddleware = require("../middleware/authMiddleware");
const protectionMiddleware = require("../middleware/protectionMiddleware");
const schemaValidation = require("../middleware/schemaValidation");
const { checkSchema } = require("express-validator");
const {
  registerValidation,
  loginValidation,
  bioValidation,
} = require("./users.schema");

userRouter.get("/rock_paper_scs", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/rock_paper_scs.html"));
});
//render register page
userRouter.get("/register", userController.renderRegister);
//render login page
userRouter.get("/login", userController.renderLogin);
//register user
userRouter.post(
  "/register",
  checkSchema(registerValidation),
  schemaValidation,
  userController.registerUser
);
//login user
userRouter.post(
  "/login",
  checkSchema(loginValidation),
  schemaValidation,
  userController.loginUser
);
//get all users
userRouter.get("/", authMiddleware, userController.getAllUsers);
//update or create userBios table
userRouter.put(
  "/detail/:idUser",
  authMiddleware,
  protectionMiddleware,
  checkSchema(bioValidation),
  schemaValidation,
  userController.createOrUpdateBio
);
//get single user Bio with specified user_id
userRouter.get(
  "/detail/:idUser",
  authMiddleware,
  protectionMiddleware,
  userController.getUserBio
);

module.exports = userRouter;
