const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");
class UserModel {
  //get user data
  getUsers = async () => {
    const userList = await db.User.findAll({ include: [db.UserBio] });
    return userList;
  };

  //   check whether user already exists or not
  isRegistered = async (username, email) => {
    const existUser = await db.User.findOne({
      where: {
        [Op.or]: [
          {
            username: username,
          },
          {
            email: email,
          },
        ],
      },
    });
    // condition when user exists or not. If exists, it will return true otherwise it will return false
    if (existUser) {
      return true;
    } else {
      return false;
    }
  };

  //   record new user
  recordUser = async (username, email, password) => {
    await db.User.create({
      username: username,
      email: email,
      password: md5(password),
    });
  };

  //login authentication
  authenticateUser = async (email, password) => {
    const sortedUsers = await db.User.findOne({
      where: {
        email: email,
        password: md5(password),
      },
      attributes: { exclude: ["password"] },
      raw: true,
    });
    console.log(sortedUsers);
    return sortedUsers;
  };

  // find user id on Users table
  findUserById = async (idUser) => {
    const sortedUsers = await db.User.findOne({
      where: {
        id: idUser,
      },
    });
    if (sortedUsers) {
      return true;
    } else {
      return false;
    }
  };

  //   find user_id in UserBios table
  getUserById = async (idUser) => {
    const sortedUsers = await db.UserBio.findOne({
      where: {
        user_id: idUser,
      },
    });
    return sortedUsers;
  };

  //   create new user Bio
  createUserBio = async (
    fullname,
    gender,
    address,
    phoneNumber,
    dateOfBirth,
    idUser
  ) => {
    await db.UserBio.create({
      fullname: fullname,
      gender: gender,
      address: address,
      phoneNumber: phoneNumber,
      dateOfBirth: dateOfBirth,
      user_id: idUser,
    });
  };

  // update user Bio
  updateUserBio = async (
    fullname,
    gender,
    address,
    phoneNumber,
    dateOfBirth,
    idUser
  ) => {
    await db.UserBio.update(
      {
        fullname: fullname,
        gender: gender,
        address: address,
        phoneNumber: phoneNumber,
        dateOfBirth: dateOfBirth,
      },
      {
        where: {
          user_id: idUser,
        },
      }
    );
  };
}
module.exports = new UserModel();
