"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class GameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      GameHistory.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "player",
      });
      GameHistory.belongsTo(models.GameRoom, {
        foreignKey: "room_id",
        as: "roomPlayed",
      });
    }
  }
  GameHistory.init(
    {
      result: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      room_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "GameHistory",
    }
  );
  return GameHistory;
};
