"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      GameRoom.belongsTo(models.User, {
        as: "player1",
        foreignKey: "player1_id",
      });
      GameRoom.belongsTo(models.User, {
        as: "player2",
        foreignKey: "player2_id",
      });
      GameRoom.hasMany(models.GameHistory, {
        as: "gameResult",
        foreignKey: "room_id",
      });
    }
  }
  GameRoom.init(
    {
      roomName: DataTypes.STRING,
      player1_id: DataTypes.INTEGER,
      player1Choice: DataTypes.STRING,
      player2_id: DataTypes.INTEGER,
      player2Choice: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "GameRoom",
    }
  );
  return GameRoom;
};
