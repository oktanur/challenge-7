"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //Users table relation with UserBios
      User.hasOne(models.UserBio, {
        foreignKey: "user_id",
      });
      //Users table relation with GameHistories
      User.hasMany(models.GameHistory, {
        as: "gameResult",
        foreignKey: "user_id",
      });
      //Users table relation with GameRooms
      User.hasMany(models.GameRoom, {
        foreignKey: "player1_id",
      });
      User.hasMany(models.GameRoom, {
        foreignKey: "player2_id",
      });
    }
  }
  User.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
