const express = require("express");
const app = express();
const PORT = 3000;
const path = require("path");
const userRouter = require("./users/users.route");
const gameRouter = require("./game/game.route");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const documSwager = require("./documSwager.json");

app.use(cors());

// ser vie engine
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
// swagger documentation
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(documSwager));
// render homepage
app.get("/", (req, res) => {
  res.sendFile("/index.html");
});
//user routing
app.use("/users", userRouter);

// game routing
app.use("/game", gameRouter);

app.listen(PORT, (err) => {
  if (err) console.log(err);
  console.log(`Server listening on http://localhost:${PORT}`);
});
