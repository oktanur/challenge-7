const express = require("express");
const gameRouter = express.Router();
const gameController = require("./game.controller");
const authMiddleware = require("../middleware/authMiddleware");
const protectionMiddleware = require("../middleware/protectionMiddleware");
const { checkSchema } = require("express-validator");
const schemaValidation = require("../middleware/schemaValidation");
const { roomValidation, updatedRoomVal } = require("./game.schema");

// create new room
gameRouter.post(
  "/room",
  authMiddleware,
  checkSchema(roomValidation),
  schemaValidation,
  gameController.createRoom
);
// update existing game room and create game history
gameRouter.put(
  "/room/:roomId",
  authMiddleware,
  checkSchema(updatedRoomVal),
  schemaValidation,
  gameController.updateRoom
);

//get user game history with specified user_id
gameRouter.get(
  "/histories/:idUser",
  authMiddleware,
  protectionMiddleware,
  gameController.getUserHistory
);

// get all rooms
gameRouter.get("/all_rooms", authMiddleware, gameController.getAllRooms);

// get specific room
gameRouter.get("/room/:roomId", authMiddleware, gameController.getGameRoom);

module.exports = gameRouter;
