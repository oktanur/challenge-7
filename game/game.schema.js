// create new room validation
const roomValidation = {
  roomName: {
    exists: {
      errorMessage: "Input room name",
      options: [
        {
          values: "falsy",
        },
      ],
    },
    isEmpty: {
      errorMessage: "Room name is empty",
      negated: true,
      options: [{ ignore_whitespace: true }],
    },
    isString: {
      negated: false,
      errorMessage: "Room name should be string",
    },
    isLength: {
      errorMessage: "Room name must contains at least 3 character",
      options: { min: 3 },
    },
  },
  player1Choice: {
    exists: {
      errorMessage: "Input player 1 choice",
      options: [
        {
          values: "falsy",
        },
      ],
    },
    isEmpty: {
      errorMessage: "Player 1 choice is empty",
      negated: true,
    },
    isIn: {
      errorMessage: "Player should be rock, papper, scissors",
      options: [["rock", "papper", "scissors"]],
    },
  },
};

// update room validation
const updatedRoomVal = {
  player2Choice: {
    exists: {
      errorMessage: "Input player 1 choice",
      options: [
        {
          values: "falsy",
        },
      ],
    },
    isEmpty: {
      errorMessage: "Player 1 choice is empty",
      negated: true,
    },
    isIn: {
      errorMessage: "Player should be rock, papper, scissors",
      options: [["rock", "papper", "scissors"]],
    },
  },
};
module.exports = { roomValidation, updatedRoomVal };
