const gameModel = require("./game.model");
class GameController {
  // create new room
  createRoom = async (req, res) => {
    try {
      // save id user from jwt token
      const player1_id = req.user.id;
      const { roomName, player1Choice } = req.body;
      // check whether room name is taken
      // if true than error message will appear, and if false then create a new room
      const takenRoom = await gameModel.checkRoomName(roomName);
      if (takenRoom) {
        return res.status(400).json({ message: "Room name is already taken" });
      } else {
        await gameModel.createNewRoom(roomName, player1_id, player1Choice);
        return res.status(200).json({ message: "New game room is created" });
      }
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };

  //update room for user player 2
  updateRoom = async (req, res) => {
    const { roomId } = req.params;
    try {
      // save id user 1 from jwt token
      const player2Id = req.user.id;
      const { player2Choice } = req.body;
      // validate room id
      const myRoom = await gameModel.findRoomById(roomId);
      if (!myRoom) {
        return res.status(400).json({ message: "Room doesn't exist" });
      }
      // if exist then check whether player 2 and player 1 are the same
      const samePlayer = await gameModel.isRoomExist(roomId, player2Id);
      if (samePlayer) {
        return res.status(400).json({ message: "You can't input choice" });
      }
      // update player 2 choice
      await gameModel.updatePlayerRoom(player2Id, player2Choice, roomId);
      // find room id in GameRoom table and also update result in GameHistories table
      const data = await gameModel.updateResult(roomId);
      console.log(data);
      return res.status(200).json({ message: "Update game room" });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };

  // Display user game history
  getUserHistory = async (req, res) => {
    try {
      const { idUser } = req.params;
      // get data on  GameHistories table with specified user_id
      const userHistoryData = await gameModel.getIdGameHistory(idUser);
      // get user id from Users table

      return res.status(200).json(userHistoryData);
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };

  // get all rooms
  getAllRooms = async (req, res) => {
    try {
      const allRooms = await gameModel.getRooms();
      return res.status(200).json(allRooms);
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "internal server error" });
    }
  };

  // get specific room
  getGameRoom = async (req, res) => {
    const { roomId } = req.params;
    try {
      // check whether room is exist
      const existRoom = await gameModel.findRoomById(roomId);
      if (existRoom) {
        const roomDetail = await gameModel.getRoomDetail(roomId);
        return res.status(200).json(roomDetail);
      } else {
        return res.status(400).json({ message: "Room not found" });
      }
    } catch {}
  };
}
module.exports = new GameController();
