const db = require("../db/models");
const { Op } = require("sequelize");
const { User, GameRoom, GameHistory } = require("../db/models");
class GameModel {
  // check room name
  checkRoomName = async (roomName) => {
    const existedRoom = await db.GameRoom.findOne({
      where: {
        roomName: roomName,
      },
    });
    if (existedRoom) {
      return true;
    } else {
      false;
    }
  };
  // create a new room
  createNewRoom = async (roomName, player1_id, player1Choice) => {
    await db.GameRoom.create({
      roomName: roomName,
      player1_id: player1_id,
      player1Choice: player1Choice,
    });
  };

  // find room by Id
  findRoomById = async (roomId) => {
    const existedRoom = await db.GameRoom.findOne({
      where: {
        id: roomId,
      },
    });
    return existedRoom;
  };
  // find existed room in GameRoom database return true if exist
  isRoomExist = async (roomId, player2Id) => {
    const existedRoom = await db.GameRoom.findOne({
      where: {
        [Op.and]: [
          {
            id: roomId,
          },
          {
            player1_id: player2Id,
          },
        ],
      },
    });
    // if room exists return true
    return existedRoom;
  };

  // update player room
  updatePlayerRoom = async (player2Id, player2Choice, roomId) => {
    await db.GameRoom.update(
      {
        player2_id: player2Id,
        player2Choice: player2Choice,
      },
      {
        where: {
          id: roomId,
        },
      }
    );
  };
  // update result in GameHistory room
  updateResult = async (roomId) => {
    const roomData = await db.GameRoom.findOne({
      where: {
        id: roomId,
      },
      raw: true,
    });
    const player1Choice = roomData.player1Choice;
    const player2Choice = roomData.player2Choice;
    const player1Id = roomData.player1_id;
    const player2Id = roomData.player2_id;
    console.log(player1Choice);
    console.log(player2Choice);
    console.log(player1Id);
    console.log(player2Id);
    console.log("roomId: " + roomId);
    // game condition
    if (player1Choice === player2Choice) {
      // create player 1 result
      await db.GameHistory.create({
        result: "draw",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "draw",
        user_id: player2Id,
        room_id: roomId,
      });
    } else if (player1Choice === "rock" && player2Choice === "papper") {
      // create player 1 result
      await db.GameHistory.create({
        result: "lose",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "win",
        user_id: player2Id,
        room_id: roomId,
      });
    } else if (player1Choice === "rock" && player2Choice === "scissors") {
      // create player 1 result
      await db.GameHistory.create({
        result: "win",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "lose",
        user_id: player2Id,
        room_id: roomId,
      });
    } else if (player1Choice === "papper" && player2Choice === "rock") {
      // create player 1 result
      await db.GameHistory.create({
        result: "win",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "lose",
        user_id: player2Id,
        room_id: roomId,
      });
    } else if (player1Choice === "papper" && player2Choice === "scissors") {
      // create player 1 result
      await db.GameHistory.create({
        result: "lose",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "win",
        user_id: player2Id,
        room_id: roomId,
      });
    } else if (player1Choice === "scissors" && player2Choice === "rock") {
      // create player 1 result
      await db.GameHistory.create({
        result: "lose",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "win",
        user_id: player2Id,
        room_id: roomId,
      });
    } else if (player1Choice === "scissors" && player2Choice === "papper") {
      // create player 1 result
      await db.GameHistory.create({
        result: "win",
        user_id: player1Id,
        room_id: roomId,
      });
      // create player 2 result
      await db.GameHistory.create({
        result: "lose",
        user_id: player2Id,
        room_id: roomId,
      });
    }
  };

  // get user id in GameHistory table
  getIdGameHistory = async (idUser) => {
    const userHistoryData = await db.GameHistory.findAll({
      include: [
        {
          model: User,
          as: "player",
          attributes: ["id", "username"],
        },
        {
          model: GameRoom,
          as: "roomPlayed",
          attributes: ["roomName"],
        },
      ],
      where: {
        user_id: idUser,
      },
    });
    return userHistoryData;
  };

  // get all rooms
  getRooms = async () => {
    const data = await db.GameRoom.findAll({
      include: [
        {
          model: GameHistory,
          as: "gameResult",
          attributes: ["result", "createdAt"],
        },
        {
          model: User,
          as: "player1",
          attributes: ["id", "username"],
        },
        {
          model: User,
          as: "player2",
          attributes: ["id", "username"],
        },
      ],
    });
    return data;
  };

  // get specific rooms
  getRoomDetail = async (roomId) => {
    const data = await db.GameRoom.findOne({
      include: [
        {
          model: User,
          as: "player1",
          attributes: ["id", "username"],
          include: [
            {
              model: GameHistory,
              as: "gameResult",
              attributes: ["result", "createdAt"],
            },
          ],
        },
        {
          model: User,
          as: "player2",
          attributes: ["id", "username"],
          include: [
            {
              model: GameHistory,
              as: "gameResult",
              attributes: ["result", "createdAt"],
            },
          ],
        },
      ],
      where: {
        id: roomId,
      },
    });
    return data;
  };
}
module.exports = new GameModel();
